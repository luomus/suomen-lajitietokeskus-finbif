#Use cases

## Pitäisikö kokoelman laatuluokituksen vaikuttaa lopputulokseen?
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/1_a.png)
Yllä lähtökohta +2 ja Riikka -1 -> +1 = todennäköinen

-----------------------

## Vaikuttaako lähtokohta mitenkään?
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/11.png)

-1 + 1 = 0

![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/13.png)

-1 + 2 = +1

-----------------------

## Arvattu nimeä näkemättä mitään
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/1.png)

Jos asiantuntija tekee näin, ei varmaan kuitenkaan pitäisi mennä todennäköiseksi vaan neutraaliksi?
Mahdollisuus asiantuntijalle määritellä kuinka varma on?

![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/14.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/15.png)

Entäs ei-asiantuntija?

-----------------------

## Ei mahdollista annotoida kahta eri lajia
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/10.png)

Jos olisi, tästä voisi syntyä kaksi eri havaintoa?

-----------------------

## Tämän pitäisi päätyä vahvistetuksi?
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/12.png)

-----------------------

## Kun joudutaan määrittämään sukutasolle..
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/16.png)

Ovatko varmoja sukutason havaintoja jne?

-----------------------

## Tää menee pieleen
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/17.png)

Ehdotettu nimeä jota ei ole -> ei vakuta. Epävarma-tagia ei millään tavalla huomioida.

-----------------------

## Tarvetta admin annotoinnille?
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/18.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/19.png)

... tällaisia kyllä voisi kuka tahansa annotoida vaikka ei olisikaan asiantuntija!

-----------------------

## Perustapauksia
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/2.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/3.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/4.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/7.png)
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/8.png)

-----------------------

## Amatööri
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/20.png)

En ilmoittautuisi matelijoiden asiantuntijaksi, mutta..

-----------------------

## Kotkan laatujutut mukaan
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/5.png)

## Joo?
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/6.png)

## Enemmän epäselvyyttä
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/9.png)

Riikan määritys "-" torpeedoi Markon määrityksen

#Muuta
##1
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_6.jpg)

##2
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_5.jpg)

##3
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_3.jpg)

##4
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_1.jpg)

##5
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_2.jpg)

##6
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_4.jpg)

##7
![case](https://bitbucket.org/luomus/eskon-dokkarit/raw/master/Lajitietokeskus/laadunvalvonta/piv_7.jpg)

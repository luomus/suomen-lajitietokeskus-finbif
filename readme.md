FinBIF Architecture and Repository Documentation
================================================

This repository contains documentation about the different open source repositories that combine FinBIF services.


Repositories
------------


### Laji.fi / Arterna.fi / Gbif.fi

#### laji.fi (frontend)
[Laji.fi frontend](https://bitbucket.org/luomus/laji.fi-front) frontend for laji.fi -website. 

#### api.laji.fi ("frontend")
[LajiApi](https://bitbucket.org/luomus/lajiapi) is the "frontend" of [api.laji.fi](https://api.laji.fi).

#### api.laji.fi (backend)
[Laji.fi backend API](https://bitbucket.org/luomus/laji.fi-backend) repository contains the frontend visible in [vanha.laji.fi](http://vanha.laji.fi) 
website and is used by api.laji.fi. This system is being converted into being just a backend API. This API provides the following FinBIF resources:

* Taxonomy, including images and descriptions
* News
* CMS content


### Data warehouse
FinBIF data warehouse combines observation and collection specimen data from many different primary data sources
to single place for easy access. System consists of the actual databases (no repositories), ETL (Extract-Transform-Load) service,
Query, GIS and Redirect services.
#### Laji-ETL / Query
[Laji-ETL](https://bitbucket.org/luomus/laji-etl) is used to get data into the data warehouse. 
It provides two methods: Push and Pull. In Push a primary system sends updates to DW's push-api whenever changes occur. In Pull a primary system implements an interface which the DW will call every now and then to get updates. In Push the primary system must make sure DW gets all the updates. In Pull the DW will make sure it reads all updates. Laji-ETL can handle multiple data formats. It converts different formats to one harmonized format for further processing.
Provides API to query contents of the Data Warehouse. 
#### FinBIF-GIS
Will provide Warehouse data as GIS services (WFS,WMS). (Also provides some other GIS-data, including 
Finnish biogeographical provinces). No code yet in any repository. Definitions will be added to a repository in the future.



### User Authentication
#### Laji-Auth
[Laji-Auth](https://bitbucket.org/luomus/laji-auth) authentication portal provides a way for users to access FinBIF services using their home
organizations identity. Currently Laji-Auth supports loging for HAKA- and VIRTU-federations, Facebook and Google authentication, LUOMUS-AD authentication
for LUOMUS workers and ability to create a custom FinBIF-account for those, that do not want to use other accounts to access FinBIF services. More
identification methods can be added in the future for example for Bird Ringers, etc.
#### Laji-Auth Client (Java)
[Java Client](https://bitbucket.org/luomus/laji-auth-client) provides an easy way to use Laji-Auth API using Java programming language.
#### Laji-Auth Client (PHP)
[PHP Client](https://bitbucket.org/luomus/laji-auth-client-php) provides an easy way to use Laji-AUTH API using PHP programming language.
#### Laji-Auth Client (Python)
Will be provided eventually.



### Multimedia service
FinBIF provides a multimedia service (that currently only supports images..) that will be provided as a service
to anyone interested in using it. Multimedia service provides a API that is used to get, add, update and remove
images. Multimedia service stores the original image (for example a large bitmap) and generates several versions
of the image (large JPG version and several different JPG thumbnails).
#### Server backend and API
[Backend](https://bitbucket.org/luomus/kuvapalvelu-server) implements the service.
#### Java Client
[Java Client](https://bitbucket.org/luomus/kuvapalvelu-client) provides an easy way to use the Multimedia-API using Java programming language.
#### PHP Client
[PHP Client](https://bitbucket.org/luomus/image-api-client-php) provides an easy way to use the Multimedia-API using PHP programming language.



### Master data management
#### Triplestore
[Triplestore](https://bitbucket.org/luomus/triplestore) is an Ontology Database that contains taxon, person, area, etc information, which together 
form FinBIF Master Data, that is used by Data Warehouse when cominging taxonomy, record owners, etc. This repository contains API that can be used
to query and alter contents of Triplestore data in rdf+xml format. Repository also contains Triplestore Editor, that is an UI for altering Triplestore
contents. Taxonomic information is modified by taxonomy experts using Taxon editor, which can also be found from this repository.  
#### Triplestore Java-Client
[Java-Client](https://bitbucket.org/luomus/triplestore-client) provides a friendly way to use Triplestore-API in Java programming language.
#### Triplestore PHP-Client
[PHP Client](https://bitbucket.org/luomus/triplestore-php) provides a friendly way to use Triplestore in PHP programming language.
#### Id-service
[URI Redirect Service](https://bitbucket.org/luomus/id) provides human and machine readable access to FinBIF resources that have URI-identifiers. 
For example if a collection specimen has an ID http://id.luomus.fi/GV.1, typing the ID to browser brings the user to this service, which redirects the user to
appropriate place, which is for example Kotka viewer for collections, Laji.fi species page for taxons (for example http://id.luomus.fi/MX.1) and so on. 



### Collection management and observation-monitoring system
#### Laji-Store
[Laji-Store](https://bitbucket.org/luomus/lajistore-api) is the database solution for FinBIF collection management (Kotka)
and observation-monitoring system (Vihko). It is also used by other primary data sources, such as Satellite Monitoring application.
It will be provided as a service for anyone interested in using Laji-Store as a database solution for their application. 
Laji-Store is a JSON Storage using new JSON capabilities of Oracle 12c.
#### Laji-Form
Laji-Form is general use form system used to transform JSON-form definitions to HTML5+JS+CSS forms.
TODO relevant repositories

#### Kotka
[Kotka](https://bitbucket.org/luomus/kotka) is FinBIF Collection management system. Currently it is a 
'monolith' application, but it is being refactored to use the different services that are listed in
this section. When completed, collection management and observation system use the same services in
slightly different fassion.

* [Elasticsearch-PHP client](https://bitbucket.org/luomus/elastic-php) is used by Kotka for searches.




### Utilities
* [html2PDF](https://bitbucket.org/luomus/html2pdf) is an utility service that can be used to turn HTML+CSS+JS into a PDF document.
* [Commons (Java)](https://bitbucket.org/luomus/luomus-commons-java) is an utility library used in some FinBIF applications.
* [Utils (Java)](https://bitbucket.org/luomus/utils) is an utility library used in some  FinBIF applications.
* [Commons (PHP)](https://bitbucket.org/luomus/common-php) is an utility library used in some FinBIF applications.



